pub fn run() -> doe::DynError {
    use cok::cli::Cli;
    use doe::*;
    use dirs::home_dir;
    let json = include_str!("../cli.json");
    let args = Cli::new(json).args();
    let arg_len = args.len();
    if let Some(home) = home_dir() {
        let password_path = home.join(".passwallet/passwords");
        let mut passwords: Vec<(String, String)> = vec![];
        if !password_path.exists() {
            std::fs::create_dir_all(home.join(".passwallet")).unwrap();
            std::fs::write(password_path.clone(), "").unwrap();
        } else {
            passwords = std::fs::read_to_string(password_path.clone())
                .unwrap()
                .split_to_vec("\n")
                .into_iter()
                .map(|s| {
                    (
                        s.split("::::::::").nth(0).unwrap().to_string(),
                        s.split("::::::::").nth(1).unwrap().to_string(),
                    )
                })
                .collect();
        }
        if arg_len == 3 && (&args[0] == "s" || &args[0] == "store") {
            let id = args.clone().get(1).unwrap().to_string();
            let password = args.clone().get(2).unwrap().to_string();
            let mut have_id = false;
            passwords.iter_mut().for_each(|s| {
                let s_0 = s.0.to_string();
                if s_0 == id {
                    have_id = true;
                    s.1 = password.to_string();
                }
            });
            if have_id {
                let mut content = "".to_string();
                passwords.into_iter().for_each(|s| {
                    content.push_str(&format!("{}::::::::{}\n", s.0, s.1));
                });
                std::fs::write(
                    &password_path.clone().display().to_string(),
                    content.as_bytes().to_vec(),
                )
                .unwrap();
            } else {
                passwords.push((id.to_string(), password.to_string()));
                let mut content = "".to_string();
                passwords.into_iter().for_each(|s| {
                    content.push_str(&format!("{}::::::::{}\n", s.0, s.1));
                });
                std::fs::write(
                    &password_path.clone().display().to_string(),
                    content.as_bytes().to_vec(),
                )
                .unwrap();
            }
            println!("store OK!");
        } else if arg_len == 1 && (&args[0] == "e" || &args[0] == "export") {
            let mut content = "".to_string();
            passwords.into_iter().for_each(|s| {
                content.push_str(&format!("{}::::::::{}\n", s.0, s.1));
            });
            std::fs::write("./passwords", content.as_bytes().to_vec()).unwrap();
        } else if arg_len == 1 && (&args[0] == "d" || &args[0] == "display") {
            for (id, name) in passwords {
                println!("{},{}", id, name);
            }
        } else if arg_len == 2 && (&args[0] == "i" || &args[0] == "import") {
            let file_name = args.clone().get(1).unwrap().to_string();
            let file_content = std::fs::read_to_string(file_name).unwrap();
            let mut file_passwords: Vec<(String, String)> = file_content
                .split_to_vec("\n")
                .into_iter()
                .map(|s| {
                    (
                        s.split("::::::::").nth(0).unwrap().to_string(),
                        s.split("::::::::").nth(1).unwrap().to_string(),
                    )
                })
                .collect();
            passwords.iter_mut().for_each(|s| {
                let s_0 = s.0.to_string();
                for (index, f_p) in file_passwords.clone().iter().enumerate() {
                    if f_p.0 == s_0 {
                        s.1 = f_p.1.to_string();
                        file_passwords.remove(index);
                    }
                }
            });
            passwords.extend(file_passwords);
            let mut content = "".to_string();
            passwords.into_iter().for_each(|s| {
                content.push_str(&format!("{}::::::::{}\n", s.0, s.1));
            });
            std::fs::write(
                &password_path.clone().display().to_string(),
                content.as_bytes().to_vec(),
            )
            .unwrap();
        }
        else if arg_len>0 {
            const HEALP:&str = r#"
pwallet

pwallet is a cli for store,export,import passwords

Usage: pwallet [OPTIONS] [COMMAND]

Commands:
pwallet store, s        Store password in pwallet       example:pwallet s my_id my_passowrd
pwallet export, e       Export passwords from pwallet   example:pwallet e
pwallet import, i       Import passwords to pwallet     example:pwallet i ./passwords
pwallet display, d      Display all passwords in terminal       example:pwallet d

pwallet -h --help       Prints help information
            "#;
            println!("{}",HEALP.trim());
        }
    } else {
        println!("Unable to determine the home directory.");
    }
    Ok(())
}
