# pwallet is a cli for store and recover passwords

[![Crates.io](https://img.shields.io/crates/v/pwallet.svg)](https://crates.io/crates/pwallet)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/pwallet)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/pwallet/-/raw/master/LICENSE)

## USEAGE:
```sh
pwallet  

pwallet is a cli for store,export,import passwords

Usage: pwallet [OPTIONS] [COMMAND]

Commands:
pwallet store, s        Store password in pwallet       example:pwallet s my_id my_passowrd
pwallet export, e       Export passwords from pwallet   example:pwallet e
pwallet import, i       Import passwords to pwallet     example:pwallet i ./passwords
pwallet display, d      Display all passwords in terminal       example:pwallet d
pwallet -h --help       Prints help information
```